<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}


try{

  $errors = FALSE;
  if (empty($_POST['fio'])) {
    print('Заполните имя.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['email'])) {
    print('Заполните E-mail.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['date'])) {
    print('Заполните дату рождения.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['field-name-2'])) {
    print('Напишите биографию.<br/>');
    $errors = TRUE;
  }

  if (empty($_POST['check-1'])) {
    print('Для продолжения ознакомьтесь с контрактом.<br/>');
    $errors = TRUE;
  }

  if ($errors) {
    exit();
  }

  $name = $_POST['fio'];
  $email = $_POST['email'];
  $date = $_POST['date'];
  $radio1 = $_POST['radio-group-1'];
  $radio2 = $_POST['radio-group-2'];
  $abilities = $_POST['field-name-4'];
  $nAbility = count($abilities);
  for($i=0; $i < 3; $i++) {
      $ab[$i] = 0;
    }
  for($i=0; $i < $nAbility; $i++) {
      $ab[$abilities[$i]] = 1;
    }
  $text = $_POST['field-name-2'];
  $checkbox = $_POST['check-1'];

  $conn = new PDO('mysql:host=localhost;dbname=u21391', 'u21391', '5188469', array(PDO::ATTR_PERSISTENT => true));


  $stmt = $conn->prepare("INSERT INTO users SET name = ?, email = ?, year = ?, gender = ?, limbs = ?, biography = ?");
  $stmt -> execute([$name, $email, $date, $radio1, $radio2, $text]);
  $id_user = $conn->lastInsertId();

  $id = $conn->prepare("INSERT INTO abilities_of_users SET id_user = ?");
  $id -> execute([$id_user]);
  $id_abil = $conn->lastInsertId();

  $ability = $conn->prepare("INSERT INTO abilities SET id_ability = ?, ability_immortality = ?, ability_passing_through_walls = ?, ability_levitation = ?");
  $ability -> execute([$id_abil, $ab[$i = 0], $ab[$i = 1], $ab[$i = 2]]);

  header('Location: ?save=1');
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
?>
